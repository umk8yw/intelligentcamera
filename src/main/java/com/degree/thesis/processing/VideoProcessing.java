package com.degree.thesis.processing;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import javax.imageio.ImageIO;

import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

public class VideoProcessing {

	public void process() throws SQLException, ParseException, IOException {
		ImageProcessing imageProcess = new ImageProcessing();

		VideoCapture video = new VideoCapture(0);
		ImgWindow wnd = ImgWindow.newWindow();
		Mat mat = new Mat();
		while (video.isOpened()) {
			video.read(mat);
			this.write(mat);
			imageProcess.processFrames(mat);
			wnd.setImage(mat);
		}
	}

	void write(Mat mat) throws IOException {
		byte[] data = new byte[mat.rows() * mat.cols() * (int) (mat.elemSize())];
		mat.get(0, 0, data);
		BufferedImage image = new BufferedImage(mat.cols(), mat.rows(), BufferedImage.TYPE_3BYTE_BGR);
		image.getRaster().setDataElements(0, 0, mat.cols(), mat.rows(), data);
		File ouptut = new File("image.png");
		ImageIO.write(image, "png", ouptut);
	}
}
