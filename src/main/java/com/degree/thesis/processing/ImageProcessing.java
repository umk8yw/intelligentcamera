package com.degree.thesis.processing;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;

import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import com.degree.thesis.model.CarDTO;
import com.degree.thesis.transaction.CarService;

/**
 * Process frames obtained from video. process method
 * 
 * @see processFrames(BufferedImage image)
 * @author alinabolindu
 */
public class ImageProcessing {

	CharacterProcessing characterRecognizer = new CharacterProcessing();

	private Logger logger = Logger.getLogger(Class.class.getName());

	CarService service = new CarService();

	/**
	 * Get the matrix of a given image.
	 * 
	 * @param image
	 *            (gray image)
	 * @return Mat
	 */
	Mat getMatrix(BufferedImage image) {
		Mat matrix = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		matrix.put(0, 0, data);
		return matrix;
	}

	/**
	 * Converts a RGB image into a GRAY image.
	 * 
	 * @param inputMatrix
	 * @return Mat
	 */
	Mat convertToGrayImage(Mat inputMatrix) {
		Mat outputMatrix = new Mat();
		Imgproc.cvtColor(inputMatrix, outputMatrix, Imgproc.COLOR_RGB2GRAY);
		return outputMatrix;
	}

	/**
	 * Converts a GRAY image into a 8-bit BINARY image.
	 * 
	 * @param inputMatrix
	 *            (gray image)
	 * @return Mat
	 * @throws IOException
	 */
	Mat applyThreshold(Mat inputMatrix) throws IOException {
		Mat outputtMatrix = new Mat();
		Mat blurImage = new Mat();
		Mat eqHist = new Mat();

		Imgproc.equalizeHist(inputMatrix, eqHist);
		Imgproc.GaussianBlur(eqHist, blurImage, new Size(31, 31), 0);
		Imgproc.adaptiveThreshold(blurImage, outputtMatrix, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
				Imgproc.THRESH_BINARY, 189, 6 /* adaptive threshold constant */);
		return outputtMatrix;
	}

	/**
	 * Detects edges of an image.
	 * 
	 * @param inputMatrix
	 *            (binary image)
	 * @return Mat
	 * @throws IOException
	 */
	Mat applyCanny(Mat inputMatrix) throws IOException {
		Mat edges = new Mat();
		Imgproc.Canny(inputMatrix, edges, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C * 2, 3,
				false);
		return edges;
	}

	/**
	 * Obtains a list of possible plates.
	 * 
	 * @param binaryImage
	 * @param grayImage
	 * @return List<Mat>
	 * @throws IOException
	 */
	List<Mat> detectPlate(Mat binaryImage, Mat grayImage) throws IOException {
		List<MatOfPoint> contours = new ArrayList<>();
		Imgproc.findContours(binaryImage, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);

		ListIterator<MatOfPoint> iterator = contours.listIterator();
		while (iterator.hasNext()) {
			MatOfPoint contour = iterator.next();
			Rect rect = Imgproc.boundingRect(contour);
			if (!this.isRectangular(rect.size().width, rect.size().height)) {
				iterator.remove();
				continue;
			}
		}
		List<Mat> contoursList = new ArrayList<>();
		for (MatOfPoint contour : contours) {
			Rect roi = Imgproc.boundingRect(contour);
			Imgproc.rectangle(grayImage, new Point(roi.x, roi.y), new Point(roi.x + roi.width, roi.y + roi.height),
					new Scalar(0, 0, 255));
			contoursList.add(new Mat(grayImage, Imgproc.boundingRect(contour)));
		}
		return contoursList;
	}

	/**
	 * Check if found contours are rectangular after a criteria.
	 * 
	 * @param width
	 * @param height
	 * @return boolean
	 */
	boolean isRectangular(double width, double height) {
		return (width / height < 5.) && (width / height > 3.) && ((width * height) > 1000);
	}

	/**
	 * Process the frames obtained from video. Converts image into a GRAY one
	 * and binaries it, applying threshold. Applies a Canny filter, and obtains
	 * regions of interest based on a criteria. Every region of interest is
	 * processed separately.
	 * 
	 * @param image
	 * @throws IOException
	 * @throws SQLException
	 * @throws ParseException
	 */

	void processFrames(Mat imageMatrix) throws IOException, SQLException, ParseException {

		Mat grayImage = this.convertToGrayImage(imageMatrix);
		Mat binaryImage = this.applyThreshold(grayImage);

		List<Mat> plateCandidates = this.detectPlate(binaryImage, grayImage);
		for (Mat plate : plateCandidates) {
			Mat binaryPlateImage = this.characterRecognizer.applyThreshold(plate, Imgproc.THRESH_BINARY_INV);
			CarDTO carPlate = this.characterRecognizer.processPlate(binaryPlateImage, plate);
			if ((carPlate != null) && (carPlate.getLocation() != null && carPlate.getNumber() != null
					&& carPlate.getPlate() != null)) {
				this.logger.info(carPlate.toString());
				this.processData(carPlate);
				break;
			}
		}
	}

	/**
	 * Persist data into database without duplicating it. If a plate already
	 * exists and it got it a second time (checks if the difference of the first
	 * registration is bigger than 3 minutes), it is considered that the car is
	 * leaving the parking. It persist the data as being not in the parking
	 * anymore.
	 * 
	 * @param carPlate
	 * @throws SQLException
	 * @throws ParseException
	 */
	void processData(CarDTO carPlate) throws SQLException, ParseException {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		List<CarDTO> carsDataList = this.service.getData();

		for (CarDTO carData : carsDataList) {
			if (carPlate.getPlate().equals(carData.getPlate())) {
				Date from = format.parse(carData.getParkedFrom());
				Date to = new Date();
				long diff = (to.getTime() - from.getTime()) / (60 * 1000) % 60;
				if (!carData.isParked() && diff > 3) {
					carData.setParkedFrom(format.format(new Date()));
					carData.setSpentTime(String.valueOf(0));
					carData.setParked(true);
					this.service.updateData(carData);
					return;
				}
				from = format.parse(carData.getParkedFrom());
				to = new Date();
				diff = (to.getTime() - from.getTime()) / (60 * 1000) % 60;
				if (diff > 3) {
					carData.setSpentTime(carData.getParkingTime(from, to));
					carData.setParked(false);
					this.service.updateData(carData);
				}
				return;
			}
		}
		carPlate.setParkedFrom(format.format(new Date()));
		carPlate.setParked(true);
		this.service.insertData(carPlate);
	}
}
