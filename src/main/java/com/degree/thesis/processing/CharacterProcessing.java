package com.degree.thesis.processing;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.ListIterator;
import java.util.logging.Logger;

import org.encog.ml.data.MLData;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.neural.networks.BasicNetwork;
import org.encog.persist.EncogDirectoryPersistence;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import com.degree.thesis.model.CarDTO;
import com.degree.thesis.neuralnetwork.NeuralNetwork;

/**
 * Process the regions of interest which are expected to be license plates
 * obtained from the main image. Finds characters from the plates and interprets
 * them.
 * 
 * @see processPlate(Mat binaryPlateImage, Mat plateImage)
 * @see recognizeCharacters(List<Mat> charactersList)
 * @author alinabolindu
 */
public class CharacterProcessing {

	BasicNetwork lettersNetwork = (BasicNetwork) EncogDirectoryPersistence.loadObject(new File(NeuralNetwork.PATH_LET));
	BasicNetwork numbersNetwork = (BasicNetwork) EncogDirectoryPersistence.loadObject(new File(NeuralNetwork.PATH_NO));

	private static final String[] LETTERS = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O",
			"P", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };

	private Logger logger = Logger.getLogger(Class.class.getName());

	/**
	 * 
	 * 
	 * @param binaryImage
	 * @param grayImage
	 * @return List<Mat>
	 * @throws IOException
	 */
	List<Mat> detectCharacters(Mat binaryImage, Mat grayImage) throws IOException {
		List<MatOfPoint> contours = new ArrayList<>();
		List<Rect> rois = new ArrayList<>();

		Imgproc.findContours(binaryImage, contours, new Mat(), Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_NONE);
		ListIterator<MatOfPoint> iterator = contours.listIterator();
		while (iterator.hasNext()) {
			MatOfPoint contour = iterator.next();
			Rect rect = Imgproc.boundingRect(contour);
			if (this.isCharacter(rect.size().width, rect.size().height, grayImage.width(), grayImage.height())) {
				rois.add(Imgproc.boundingRect(contour));
				iterator.remove();
			}
		}
		rois.addAll(this.getRestOfRois(contours, rois));
		Collections.sort(rois, (a, b) -> Double.valueOf(a.tl().x).compareTo(Double.valueOf(b.tl().x)));

		List<Mat> characters = new ArrayList<>();
		for (Rect roi : rois) {
			// this.logger.info("Detected character: " + roi.height + "," +
			// roi.width);
			Mat mat = new Mat(grayImage, roi);
			Mat binary = this.applyThreshold(mat, Imgproc.THRESH_BINARY);
			characters.add(binary);
			Imgproc.rectangle(grayImage, new Point(roi.x, roi.y), new Point(roi.x + roi.width, roi.y + roi.height),
					new Scalar(0, 0, 255));
		}
		return characters;
	}

	boolean isCharacter(double width, double height, int plateWidth, int plateHeight) {
		if (width > 50 && height > 50) {
			double heigthRatio = height / plateHeight;
			double widthRatio = width / plateWidth;
			return (heigthRatio > 0.5 && heigthRatio < 0.9) && (widthRatio > 0.05 && widthRatio < 0.3);
		}
		return false;
	}

	List<Rect> getRestOfRois(List<MatOfPoint> allContours, List<Rect> roi) {
		List<Rect> restOfRois = new ArrayList<>();
		if (!roi.isEmpty()) {
			int minHeight = this.getMinHeight(roi);
			for (MatOfPoint contour : allContours) {
				Rect rect = Imgproc.boundingRect(contour);
				if ((rect.height > (minHeight - 2)) && (rect.height < (minHeight + 2))) {
					restOfRois.add(Imgproc.boundingRect(contour));
				}
			}
		}
		return restOfRois;
	}

	int getMinHeight(List<Rect> contours) {
		if (contours.isEmpty())
			return 0;
		int min = contours.get(0).height;
		for (int i = 1; i < contours.size(); i++) {
			min = contours.get(i).height < min ? contours.get(i).height : min;
		}
		return min;
	}

	List<Mat> normalizeCharacters(List<Mat> characters) throws IOException {
		List<Mat> normalizedCharacters = new ArrayList<>();
		for (Mat character : characters) {
			Mat normalizedChar = new Mat();
			Imgproc.resize(character, normalizedChar, new Size(80, 100));
			normalizedCharacters.add(normalizedChar);
		}
		return normalizedCharacters;
	}

	Mat applyThreshold(Mat image, int type) {
		Mat binaryImage = new Mat();
		Imgproc.adaptiveThreshold(image, binaryImage, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C, type, 49,
				12 /* adaptive threshold constant */);
		return binaryImage;
	}

	CarDTO recognizeCharacters(List<Mat> charactersList) {
		CarDTO plate = new CarDTO();
		List<String> characters = new ArrayList<>();

		characters.add(this.computeLetter(new BasicMLData(this.getVector(charactersList.get(0)))));
		characters.add(this.computeLetter(new BasicMLData(this.getVector(charactersList.get(1)))));
		characters.add(this.computeNumber(new BasicMLData(this.getVector(charactersList.get(2)))));
		characters.add(this.computeNumber(new BasicMLData(this.getVector(charactersList.get(3)))));
		characters.add(this.computeLetter(new BasicMLData(this.getVector(charactersList.get(4)))));
		characters.add(this.computeLetter(new BasicMLData(this.getVector(charactersList.get(5)))));
		characters.add(this.computeLetter(new BasicMLData(this.getVector(charactersList.get(6)))));

		if (characters.get(0) != null && characters.get(1) != null) {
			plate.setLocation(characters.get(0) + characters.get(1));
		}
		if (characters.get(2) != null && characters.get(3) != null) {
			plate.setNumber(characters.get(2) + characters.get(3));
		}
		if (characters.get(4) != null && characters.get(5) != null && characters.get(6) != null) {
			plate.setPlate(characters.get(0) + characters.get(1) + " " + characters.get(2) + characters.get(3) + " "
					+ characters.get(4) + characters.get(5) + characters.get(6));
		}
		return plate;
	}

	String computeLetter(MLData data) {
		double max = 0.0;
		int index = 0;
		final MLData output = this.lettersNetwork.compute(data);
		for (int i = 0; i < 25; i++) {
			if (max < output.getData(i)) {
				max = output.getData(i);
				index = i;
			}
		}
		return CharacterProcessing.LETTERS[index];
	}

	String computeNumber(MLData data) {
		double max = 0.0;
		int index = 0;
		final MLData output = this.numbersNetwork.compute(data);
		for (int i = 0; i < 10; i++) {
			if (max < output.getData(i)) {
				max = output.getData(i);
				index = i;
			}
		}
		return String.valueOf(index);
	}

	double[] getVector(Mat matrix) {
		String[] matrixArray = matrix.dump().replace("[", "").replace(";", ",").replace("]", "").split(",");
		double[] vector = new double[matrixArray.length];
		for (int j = 0; j < matrixArray.length; j++) {
			vector[j] = Double.parseDouble(matrixArray[j]);
		}
		return vector;
	}

	CarDTO processPlate(Mat binaryPlateImage, Mat plateImage) throws IOException {
		List<Mat> characters = this.detectCharacters(binaryPlateImage, plateImage);
		if (characters.size() > 6) {
			// this.logger.info("Detected plate: " + plateImage.height() + ", "
			// + plateImage.width());
			List<Mat> normalizedCharacters = this.normalizeCharacters(characters);
			return this.recognizeCharacters(normalizedCharacters);
		}
		return null;
	}
}
