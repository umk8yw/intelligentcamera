package com.degree.thesis.processing;

public interface ValueCallback {
	public void valueChanged(int value);
}
