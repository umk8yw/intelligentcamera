package com.degree.thesis;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.degree.thesis.model.CarDTO;
import com.degree.thesis.transaction.CarService;

public class DemoServlet extends HttpServlet {

	private static final long serialVersionUID = 1367003098805036827L;

	@Override
	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");// setting the content type
		PrintWriter pw = res.getWriter();// get the stream to write the data

		CarService service = new CarService();

		List<CarDTO> dataList = service.getData();

		pw.println(
				"<table class=\"w3\" style=\"width:100%\"> <tbody> <tr> <th>Plate</th> <th>From</th> <th>Time spent</th> <th>Parked</th> </tr>");

		for (CarDTO data : dataList) {
			pw.println("<tr> <th>" + data.getPlate() + "</th> <th>" + data.getParkedFrom() + "</th> <th>"
					+ data.getSpentTime() + "</th> <th>" + data.isParked() + "</th> </tr>");
		}
		pw.println("</tbody></table>");
		pw.close();// closing the stream
	}
}
