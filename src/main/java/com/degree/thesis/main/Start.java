package com.degree.thesis.main;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;

import org.opencv.core.Core;

import com.degree.thesis.processing.VideoProcessing;

public class Start {

	public static void main(String[] args) throws IOException {

		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		try {
			new VideoProcessing().process();
		} catch (IOException | SQLException | ParseException e) {
			e.printStackTrace();
		}
	}
}
