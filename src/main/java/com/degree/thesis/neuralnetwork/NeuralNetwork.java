package com.degree.thesis.neuralnetwork;

import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.encog.persist.EncogDirectoryPersistence;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

public class NeuralNetwork {

	public static final String PATH_NO = "./config_no.txt";
	public static final String PATH_LET = "./config_let.txt";

	private Logger logger = Logger.getLogger(Class.class.getName());
	int index = 0;

	double[][] getInputForNumberTraining() throws IOException {
		double[][] data = new double[73][8000];
		this.addData(data, "./no/0/", 9);
		this.addData(data, "./no/1/", 11);
		this.addData(data, "./no/2/", 5);
		this.addData(data, "./no/3/", 4);
		this.addData(data, "./no/4/", 10);
		this.addData(data, "./no/5/", 11);
		this.addData(data, "./no/6/", 5);
		this.addData(data, "./no/7/", 7);
		this.addData(data, "./no/8/", 6);
		this.addData(data, "./no/9/", 5);
		return data;
	}

	double[][] getInputForLetters() throws IOException {
		double[][] data = new double[182][8000];
		this.addData(data, "./let/A/", 6);
		this.addData(data, "./let/B/", 15);
		this.addData(data, "./let/C/", 8);
		this.addData(data, "./let/D/", 5);
		this.addData(data, "./let/E/", 5);
		this.addData(data, "./let/F/", 7);
		this.addData(data, "./let/G/", 10);
		this.addData(data, "./let/H/", 7);
		this.addData(data, "./let/I/", 5);
		this.addData(data, "./let/J/", 3);
		this.addData(data, "./let/K/", 5);
		this.addData(data, "./let/L/", 10);
		this.addData(data, "./let/M/", 5);
		this.addData(data, "./let/N/", 5);
		this.addData(data, "./let/O/", 12);
		this.addData(data, "./let/P/", 3);
		this.addData(data, "./let/R/", 8);
		this.addData(data, "./let/S/", 6);
		this.addData(data, "./let/T/", 8);
		this.addData(data, "./let/U/", 10);
		this.addData(data, "./let/V/", 15);
		this.addData(data, "./let/W/", 8);
		this.addData(data, "./let/X/", 4);
		this.addData(data, "./let/Y/", 7);
		this.addData(data, "./let/Z/", 5);
		return data;
	}

	void addData(double[][] data, String path, int max) throws IOException {
		for (int i = 1; i <= max; i++) {
			double[] vect = this.getVector(path, i);
			data[this.index] = vect;
			this.index++;
		}
	}

	double[] getVector(String path, int i) throws IOException {
		BufferedImage image = ImageIO.read(new File(path + i + ".png"));
		Mat matrix = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC1);
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		matrix.put(0, 0, data);

		String[] matrixArray = matrix.dump().replace("[", "").replace(";", ",").replace("]", "").split(",");

		double[] vector = new double[matrixArray.length];
		for (int j = 0; j < matrixArray.length; j++) {
			vector[j] = Double.parseDouble(matrixArray[j]);
		}
		return vector;
	}

	void trainForNumbers(double[][] inputData, double[][] idealData) {
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, 8000));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), true, 2000));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 10));
		network.getStructure().finalizeStructure();
		network.reset();

		MLDataSet trainingSet = new BasicMLDataSet(inputData, idealData);
		final ResilientPropagation train = new ResilientPropagation(network, trainingSet);

		int epoch = 1;
		do {
			train.iteration();
			this.logger.info("Epoch#" + epoch + "Error:" + train.getError());
			epoch++;
		} while (train.getError() > 0.0001);

		train.finishTraining();
		EncogDirectoryPersistence.saveObject(new File(NeuralNetwork.PATH_NO), network);
		Encog.getInstance().shutdown();
	}

	void trainForLetters(double[][] inputData, double[][] idealData) {
		BasicNetwork network = new BasicNetwork();
		network.addLayer(new BasicLayer(null, true, 8000));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), true, 3000));
		network.addLayer(new BasicLayer(new ActivationSigmoid(), false, 25));
		network.getStructure().finalizeStructure();
		network.reset();

		MLDataSet trainingSet = new BasicMLDataSet(inputData, idealData);
		final ResilientPropagation train = new ResilientPropagation(network, trainingSet);

		int epoch = 1;
		do {
			train.iteration();
			this.logger.info("Epoch#" + epoch + "Error:" + train.getError());
			epoch++;
		} while (train.getError() > 0.0001);

		train.finishTraining();
		EncogDirectoryPersistence.saveObject(new File(NeuralNetwork.PATH_LET), network);
		Encog.getInstance().shutdown();
	}
}
