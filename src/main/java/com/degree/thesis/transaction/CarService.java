package com.degree.thesis.transaction;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.degree.thesis.model.CarDTO;

/**
 * Start first the server with "mysql.server start" on terminal.
 * 
 * @author alinabolindu
 */
public class CarService {

	private static final String DB_URL = "jdbc:mysql://localhost/parking_cars?"
			+ "useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

	private static final String USER = "root";

	private static final String PASS = "Parola m3a ";

	private Logger logger = Logger.getLogger(Class.class.getName());

	Connection getManager()
			throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {

		Class.forName("com.mysql.jdbc.Driver").newInstance();
		return DriverManager.getConnection(DB_URL, USER, PASS);
	}

	public List<CarDTO> getData() {
		List<CarDTO> carsDataList = new ArrayList<>();
		final String select = "SELECT cars.id,cars.from_date,cars.time,cars.parked FROM parking_cars.cars";
		try (ResultSet rs = this.getManager().createStatement().executeQuery(select)) {

			while (rs.next()) {
				CarDTO carData = new CarDTO();
				carData.setPlate(rs.getString("id"));
				carData.setParkedFrom(rs.getString("from_date"));
				carData.setSpentTime(rs.getString("time"));
				carData.setParked(rs.getBoolean("parked"));
				carsDataList.add(carData);
			}
		} catch (Exception e) {
			this.logger.log(null, e.getMessage(), e);
		}
		return carsDataList;
	}

	public void insertData(CarDTO carData) throws SQLException {
		final String insert = "INSERT INTO parking_cars.cars VALUES ('" + carData.getPlate() + "','"
				+ carData.getLocation() + "','" + carData.getNumber() + "','" + carData.getParkedFrom() + "'," + 0 + ","
				+ carData.isParked() + ");";
		Connection connection = null;
		Statement statement = null;
		try {
			connection = this.getManager();
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			statement.executeUpdate(insert);
			connection.commit();
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			if (connection != null) {
				connection.rollback();
			}
			this.logger.log(null, e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}

	public void updateData(CarDTO carData) throws SQLException {
		final String update = "UPDATE parking_cars.cars SET cars.from_date = '" + carData.getParkedFrom()
				+ "',cars.time = '" + carData.getSpentTime() + "', cars.parked = " + carData.isParked()
				+ " WHERE cars.id = '" + carData.getPlate() + "'";
		Connection connection = null;
		Statement statement = null;
		try {
			connection = this.getManager();
			connection.setAutoCommit(false);
			statement = connection.createStatement();
			statement.executeUpdate(update);
			connection.commit();
		} catch (SQLException | InstantiationException | IllegalAccessException | ClassNotFoundException e) {
			if (connection != null) {
				connection.rollback();
			}
			this.logger.log(null, e.getMessage(), e);
		} finally {
			if (statement != null) {
				statement.close();
			}
			if (connection != null) {
				connection.close();
			}
		}
	}
}
