package com.degree.thesis.model;

import java.util.Date;

public class CarDTO {

	private String plate;
	private String location;
	private String number;
	private String parkedFrom;
	private String parkedTo;
	private String spentTime;
	private boolean parked;

	public String getPlate() {
		return this.plate;
	}

	public void setPlate(String plate) {
		this.plate = plate;
	}

	public String getLocation() {
		return this.location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getNumber() {
		return this.number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public String getParkedFrom() {
		return this.parkedFrom;
	}

	public void setParkedFrom(String parkedFrom) {
		this.parkedFrom = parkedFrom;
	}

	public String getParkedTo() {
		return this.parkedTo;
	}

	public void setParkedTo(String parkedTo) {
		this.parkedTo = parkedTo;
	}

	public String getSpentTime() {
		return this.spentTime;
	}

	public void setSpentTime(String spentTime) {
		this.spentTime = spentTime;
	}

	public boolean isParked() {
		return this.parked;
	}

	public void setParked(boolean parked) {
		this.parked = parked;
	}

	public String getParkingTime(Date parkedFrom, Date parkedTo) {
		long diff = parkedTo.getTime() - parkedFrom.getTime();
		long diffSeconds = diff / 1000 % 60;
		long diffMinutes = diff / (60 * 1000) % 60;
		long diffHours = diff / (60 * 60 * 1000) % 24;
		return diffHours + " hours, " + diffMinutes + " minutes, " + diffSeconds + " seconds";
	}

	@Override
	public String toString() {
		return "LICENCE PLATE: [ " + this.plate + " ]";
	}
}